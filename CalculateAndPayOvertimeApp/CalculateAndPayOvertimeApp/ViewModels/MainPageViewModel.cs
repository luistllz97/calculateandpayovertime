﻿using CalculateAndPayOvertimeApp.Models;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace CalculateAndPayOvertimeApp.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        #region Props
        readonly int DayByMonths = 30;
        readonly int LaboralHours = 8;
        int _Salary;
        public int Salary { get => _Salary; set => SetProperty(ref _Salary, value); }
        SalaryModel _SalaryInfo = new SalaryModel();
        public SalaryModel SalaryInfo { get => _SalaryInfo; set => SetProperty(ref _SalaryInfo, value); }
        int _ExtraHours;
        public int ExtraHours { get => _ExtraHours; set => SetProperty(ref _ExtraHours, value); }
        public ObservableCollection<SalaryModel> ListOfSalary { get; set; } = new ObservableCollection<SalaryModel>();
        #endregion

        #region Commands
        public DelegateCommand ToCalculateCommand { get; set; }
        public DelegateCommand ClearCommand { get; set; }
        #endregion

        public MainPageViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            Title = "Calculate and pay overtime";
            ToCalculateCommand = new DelegateCommand(Calculate);
            ClearCommand = new DelegateCommand(Clear);
        }

        #region Acctions
        public void Calculate()
        {
            SalaryInfo.NormalSalary = Salary;
            SalaryInfo.NomalDay = Salary / DayByMonths;
            SalaryInfo.NormalHour = SalaryInfo.NomalDay / LaboralHours;
            int Multiple = (ExtraHours > 3) ? 3 : 2;
            SalaryInfo.ExtraHourPay = SalaryInfo.NormalHour * Multiple;
            SalaryInfo.ExtraDayPay = SalaryInfo.ExtraHourPay * ExtraHours;
            SalaryInfo.Total = Salary + (SalaryInfo.ExtraDayPay * 30);
            ListOfSalary.Add(SalaryInfo);
        }
        public void Clear() =>
            ListOfSalary.Clear();
        #endregion
    }
}
