﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CalculateAndPayOvertimeApp.Models
{
    public class SalaryModel
    {
        public int NomalDay { get; set; }
        public int NormalHour { get; set; }
        public int NormalSalary { get; set; }
        public int ExtraHourPay { get; set; }
        public int ExtraDayPay { get; set; }
        public int Total { get; set; }
    }
}
